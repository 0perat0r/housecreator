# HouseCreator

This java program creates houses (suited for model railroads) as OpenSCAD files from a JSON-configuration and template files.

Dieses Java-Projekt erzeugt aus JSON-Konfigurationen und Templates Häuser (für Modellbahnen) als OpenSCAD-Dateien.