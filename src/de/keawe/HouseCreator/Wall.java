package de.keawe.HouseCreator;

import java.util.Vector;

import javax.naming.ConfigurationException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class Wall {
	Vector<Opening> openings;
	private Double thickness = null;
	private String visibility = "visible";

	public Wall(JSONObject json) throws ConfigurationException {
		for (Object key : json.keySet()) {
			if (key instanceof String) {
				String keyString = (String) key;
				Object val = json.get(key);
				switch (keyString) {
					case "openings":
						loadOpenings((JSONArray) val);
						break;
					case "thickness":
						thickness  = Double.parseDouble(""+val);
						break;
					case "visibility":
						visibility = val.toString();
						break;
					default:
						System.out.println("Found unknwon tag in Wall code: "+key);
				}
			}
		}
	}

	private void loadOpenings(JSONArray json) throws ConfigurationException {
		openings = new Vector<>();
		for (Object elem:json) openings.add(Opening.load((String)elem));
	}
	
	public String scadMinus(double defaultWallThickness, double length, double height) {
		if (thickness == null) thickness = defaultWallThickness;
		StringBuffer sb = new StringBuffer();
		if (openings.size()>0) {
			double dist = length/(openings.size());
			double x = (dist-length) / 2;
			for (Opening opening:openings) {
				if (opening.ready()) sb.append("translate(["+x+",0,0]) "+opening.minus(thickness,height)+"\n\t");
				x+=dist;
			}
		}
		return sb.toString();
	}
	public StringBuffer scadPlus(double defaultWallThickness, double length, double height) {
		StringBuffer sb = new StringBuffer();
		System.out.println("Wall.thickness = "+thickness);
		if (thickness == null) thickness = defaultWallThickness;
		System.out.println("Drawing wall with thickness: "+thickness);
		sb.append("cube(["+(length+thickness)+", "+thickness+", "+height+"],true);");
		if (openings.size()>0) {
			double dist = length/(openings.size());
			double x = (dist-length) / 2;
			for (Opening opening:openings) {
				if (opening.ready()) sb.append("\n	translate(["+x+",0,0]) "+opening.plus(thickness,height));
				x+=dist;
			}
		}
		return sb;
	}

	public boolean visible() {
		return !visibility.equals("hidden");
	}
}
