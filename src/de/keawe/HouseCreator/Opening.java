package de.keawe.HouseCreator;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.nio.file.NoSuchFileException;
import java.util.HashMap;

import javax.naming.ConfigurationException;



public class Opening {
	private static String path = ".";
	private static HashMap<String, Opening> registry = new HashMap<>();
	private StringBuffer plus = new StringBuffer();
	private StringBuffer minus = new StringBuffer();
	private String name;

	public Opening(String elementName) throws ConfigurationException {
		name = elementName;
		if (!name.isEmpty()) try {
			String filename = path+"/"+name+".scad";
			File file = new File(filename);
			byte mode = 0;
			
			if (!file.exists()) throw new NoSuchFileException(filename);
			BufferedReader br = new BufferedReader(new FileReader(file));
			String line;
			while ((line = br.readLine()) != null) {
				if (line.isEmpty()) continue;
				if (line.startsWith("module plus")) {
					line = line.replace("module plus", "module "+name+"_plus");
					mode=1;
				}
				if (line.startsWith("module minus")) {
					line = line.replace("module minus", "module "+name+"_minus");
					mode=2;
				}
				if (line.startsWith("difference")) mode=3;
				switch (mode) {
					case 1:
						plus.append(line+"\n");
						break;
					case 2:
						minus.append(line+"\n");
						break;
				}
			}
			br.close();
			if (plus.length()<1) throw new ConfigurationException(filename+" missing parts to add.");
			if (minus.length()<1) throw new ConfigurationException(filename+" missing parts to subtract.");
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}
	}

	public static void setPath(String searchPath) {
		Opening.path  = searchPath;
	}

	public static Opening load(String elem) throws ConfigurationException {
		Opening opening = registry.get(elem);
		if (opening == null) {
			opening = new Opening(elem);
			registry.put(elem, opening);
		}
		return opening;
	}

	public static StringBuffer moduleCodes() {
		StringBuffer sb = new StringBuffer();
		for (String key : registry.keySet()) {
			Opening op = registry.get(key);
			sb.append(op.plus+"\n");
			sb.append(op.minus+"\n");
		}
		return sb;
	}

	public String minus(double thickness,double height) {
		return name+"_minus("+thickness+", "+height+");";
	}

	public String plus(double thickness, double height) {
		return name+"_plus("+thickness+", "+height+");";
	}

	public boolean ready() {
		return !name.isEmpty();
	}
}
