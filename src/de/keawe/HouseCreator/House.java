package de.keawe.HouseCreator;

import java.awt.geom.Point2D;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.security.InvalidParameterException;
import java.util.HashMap;
import java.util.Set;
import java.util.Vector;

import javax.naming.ConfigurationException;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class House {
	
	private double scale = 1000;
	
	public House(JSONObject json) throws ConfigurationException {
		double defaultWallThickness = 0.4;
		for (Object key: json.keySet()) {
			if (key instanceof String) {
				String keyString = (String) key;
				
				Object val = json.get(key);
				
				switch (keyString) {
					case "comment":
						System.out.println(json.get(keyString));
						break;
					case "scale":
						scale  = Double.parseDouble(""+val);
						break;
					case "wall-thickness":
						defaultWallThickness  = Double.parseDouble(""+val);
						break;
					default:
						if (val instanceof JSONObject) HousePart.register(new HousePart(keyString,(JSONObject) val));
				}
			}
		}
		HousePart.setDefaultWallThickness(defaultWallThickness);
	}
	
	@Override
	public String toString() {
		StringBuffer buf = new StringBuffer();
		buf.append("House with the following parts:\n");
		for (HousePart part:HousePart.list()) buf.append(part+"\n");
		return buf.toString();
	}

	public static void main(String[] args) {
		if (args.length<1) throw new InvalidParameterException("You need to pass a filename (config file in JSON format)!");
		File jsonFile = new File(args[0]);
		if (!jsonFile.exists()) throw new InvalidParameterException("The specified file does not exist!");
		try {
			System.out.println("Loading "+jsonFile+": ");
			JSONParser parser = new JSONParser();
			Object json = parser.parse(new FileReader(jsonFile));
			if (json instanceof JSONObject) {
				String path = jsonFile.getParentFile().getAbsolutePath();
				HouseLevel.setPath(path);
				Opening.setPath(path);
				RoofShape.setPath(path);
				try {
				House house = new House((JSONObject) json);
				String code = house.scadCode().toString();
				File scadFile = new File(args[0].replace(".json",".scad"));
				FileWriter out=new FileWriter(scadFile);				
				out.write(code);
				out.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private StringBuffer scadCode() {
		StringBuffer code = new StringBuffer();
		code.append("$fn=32;\n");
		code.append(Opening.moduleCodes());
		code.append(RoofShape.codes());
		code.append(House.roofDefinitions());
		code.append(HouseLevel.fillDefinitions());
		if (scale != 1000) code.append("scale(["+1000/scale+","+1000/scale+","+1000/scale+"]) ");
		code.append("color([0.9,0.9,0.9]) difference(){\n");
		code.append("	union(){\n");
		code.append("		"+scadPlus().replace("\n", "\n\t\t").trim());
		code.append("\n	} // end of union. subtracted parts follow:\n");
		code.append("	"+scadMinus().replace("\n","\n\t").trim());
		code.append("\n}");
		return code;
	}
	
	private static Object roofDefinitions() {
		StringBuffer sb = new StringBuffer();
		for (HousePart part:HousePart.list()) sb.append(part.roofDefinition());
		return sb;
	}

	private String scadMinus() {
		StringBuffer code = new StringBuffer();
		HousePart main = HousePart.main();
		for (HousePart adj:main.adjacent()) {
			code.append("\ntranslate(["+adj.x()+", "+adj.y()+", 0]) { // "+adj.name()+"\n\t");
			code.append(adj.scadMinus().replace("\n", "\n\t").trim());
			code.append("\n}");
		}
		return code.toString();
	}

	private String scadPlus() {
		StringBuffer code = new StringBuffer();
		HousePart main = HousePart.main();
		for (HousePart adj:main.adjacent()) {
			if (!adj.visible()) continue; 
			code.append("translate(["+adj.x()+", "+adj.y()+", 0]) { // "+adj.name()+"\n\t");
			code.append(adj.scadPlus().replace("\n", "\n\t").trim());
			code.append("\n}");
			
		}
		return code.toString();
	}
}
