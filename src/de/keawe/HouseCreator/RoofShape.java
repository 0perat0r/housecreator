package de.keawe.HouseCreator;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.TreeMap;

import org.json.simple.JSONObject;

import com.sun.org.apache.bcel.internal.generic.Type;

public class RoofShape {
	private static String path;
	private double length;
	private double width;
	private static TreeMap<String, String> codes = new TreeMap<>();
	private String type;
	private double overlap = 0;

	public RoofShape(double height, JSONObject json) {
		for (Object key:json.keySet()) {
			if (key instanceof String) {
				String keyString = (String) key;
				Object val = json.get(key);
				switch (keyString) {
					case "overlap":
						overlap  = Double.parseDouble(""+val);
						break;
					case "type":
						type = val.toString(); 
						RoofShape.loadCode(type);
						break;
					default:
						System.out.println("RoofShape.new encounterd unknown tag: "+key);
				}
				
			}
		}
	}

	private static void loadCode(String codeName) {
		String code = codes.get(codeName);
		if (code == null) {
			String filename = path+"/"+codeName+".scad";
			File file = new File(filename);
			try {
				if (!file.exists()) throw new FileNotFoundException(filename);
				StringBuffer sb = new StringBuffer();
				BufferedReader br = new BufferedReader(new FileReader(file));
				String line;
				while ((line = br.readLine())!=null) {
					if (line.isEmpty()) continue;
					if (line.startsWith("width")) continue;
					if (line.startsWith("height")) continue;
					if (line.startsWith("length")) continue;
					sb.append(line+"\n");
				}
				codes.put(codeName, sb.toString());
				br.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	public static void setPath(String path) {
		RoofShape.path = path;
	}

	public String scad(double height) {
		return type+"("+(length)+", "+(width)+", "+height+");";
	}

	public static StringBuffer codes() {
		StringBuffer sb = new StringBuffer();
		for (String codeName:codes.keySet()) {
			sb.append("module "+codeName+"(length, width, height){\n\t");
			sb.append(codes.get(codeName).replace("\n", "\n\t").trim());
			sb.append("\n}\n");
		}
		return sb;
	}

	public void setLength(double length) {
		this.length = length;
	}

	public void setWidth(double width) {
		this.width = width;
	}

	public double overlap() {
		return overlap;
	}

	public double length() {
		return length;
	}
	
	public double width() {
		return width;
	}
}
