package de.keawe.HouseCreator;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.HashMap;

import javax.naming.ConfigurationException;

import org.json.simple.JSONObject;

public class HouseLevel {
	
	private String name = "unnamed level";
	private double height = 0;
	private Wall northWall;
	private Wall eastWall;
	private Wall southWall;
	private Wall westWall;
	private double length;
	private double width;
	private double defaultWallThickness;
	private String visibility = "visible";
	private String fill = null;
	private static HashMap<String,StringBuffer> fills = new HashMap<>();
	private static String path;

	public HouseLevel(double length, double width, JSONObject json) throws ConfigurationException {
		this.length = length;
		this.width = width;
		for (Object key : json.keySet()) {
			if (key instanceof String) {
				String keyString = (String) key;
				Object val = json.get(key);
				switch (keyString) {
					case "name":
						name = (String)val;
						break;
					case "height":
						height  = Double.parseDouble(""+val); 
						break;
					case "north-wall":
						northWall = new Wall((JSONObject) val);
						break;
					case "east-wall":
						eastWall = new Wall((JSONObject) val);
						break;
					case "south-wall":
						southWall = new Wall((JSONObject) val);
						break;
					case "west-wall":
						westWall = new Wall((JSONObject) val);
						break;
					case "comment":
						System.out.println(val);
						break;
					case "visibility":
						visibility  = val.toString();
						break;
					case "fill":
						fill  = val.toString();
						loadFill(fill);
						
						break;
					default:
						System.out.println("Encountered unknown "+keyString+" tag in HouseLevel.");
				}
			}
		}
		if (southWall==null) throw new ConfigurationException("No south wall defined for "+name);
		if (northWall==null) throw new ConfigurationException("No south wall defined for "+name);
		if (eastWall==null) throw new ConfigurationException("No south wall defined for "+name);
		if (westWall==null) throw new ConfigurationException("No south wall defined for "+name);
	}
	
	private void loadFill(String name) {
		if (!fills.containsKey(name)) {
			File file = new File(path+"/"+name+".scad");
			try {
				if (!file.exists()) throw new FileNotFoundException(file.toString());
				BufferedReader br = new BufferedReader(new FileReader(file));
				String line;
				StringBuffer buf = new StringBuffer();
				boolean active = false;
				while ((line = br.readLine()) != null) {
					if (line.contains("module") && line.contains("inner_walls")) {
						active = true;
						line = line.replace("inner_walls", name+"_walls");
					}
					if (active) buf.append(line+"\n");
					if (line.startsWith("}")) active = false;
				}
				fills.put(name, buf);
			} catch (Exception e) {
				e.printStackTrace();
				return;
			}
		}
	}
	
	@Override
	public String toString() {
		return name+" ("+height+"m heigh)";
	}
	
	public String scadMinus() {
		StringBuffer sb = new StringBuffer();
		
		if (northWall.visible()) {
			sb.append("translate([0,"+(width/2)+",0]) { // north wall \n\t");
			sb.append(northWall.scadMinus(defaultWallThickness,length,height).trim());
			sb.append("\n}");
		}
		
		if (eastWall.visible()) {
			sb.append("\ntranslate(["+(length/2)+",0 ,0]) rotate([0,0,-90]) { // east wall\n\t");
			sb.append(eastWall.scadMinus(defaultWallThickness,width,height).trim());
			sb.append("\n}");
		}
		
		if (southWall.visible()) {
			sb.append("\ntranslate([0,"+(-width/2)+",0]) rotate([0,0,180]) { // south wall\n\t");
			sb.append(southWall.scadMinus(defaultWallThickness,length,height).trim());
			sb.append("\n}");
		}
		
		if (westWall.visible()) {
			sb.append("\ntranslate(["+(-length/2)+",0 ,0]) rotate([0,0,90]) { // west wall\n\t");
			sb.append(westWall.scadMinus(defaultWallThickness,width,height).trim());
			sb.append("\n}");
		}
		return sb.toString();
	}

	public String scadPlus() {
		StringBuffer sb = new StringBuffer();
		sb.append("translate([0,0,"+(0.025-height/2)+"]) cube(["+length+","+width+", 0.05],true);\n");
		
		if (northWall.visible()) {
			sb.append("translate([0,"+(width/2)+",0]) { // north wall \n\t");
			sb.append(northWall.scadPlus(defaultWallThickness,length,height));
			sb.append("\n}");
		}
		
		if (eastWall.visible()) {
			sb.append("\ntranslate(["+(length/2)+",0 ,0]) rotate([0,0,-90]) { // east wall\n\t");
			sb.append(eastWall.scadPlus(defaultWallThickness,width,height));
			sb.append("\n}");
		}
		
		if (southWall.visible()) {
			sb.append("\ntranslate([0,"+(-width/2)+",0]) rotate([0,0,180]) { // south wall\n\t");
			sb.append(southWall.scadPlus(defaultWallThickness,length,height));
			sb.append("\n}");
		}
		
		if (westWall.visible()) {
			sb.append("\ntranslate(["+(-length/2)+",0 ,0]) rotate([0,0,90]) { // west wall\n\t");
			sb.append(westWall.scadPlus(defaultWallThickness,width,height));
			sb.append("\n}");
		}
		
		if (fill!=null) sb.append(fill+"_walls("+length+","+width+","+height+","+defaultWallThickness+");");
		return sb.toString();
	}

	public double height() {
		return this.height;
	}

	public String name() {
		return name;
	}

	public void setDefaultWallThickness(double defaultWallThickness) {
		this.defaultWallThickness = defaultWallThickness;
		
	}

	public boolean visible() {
		return !visibility.equals("hidden");
	}
	public static void setPath(String path) {
		HouseLevel.path = path;			
	}

	public static StringBuffer fillDefinitions() {
		StringBuffer sb = new StringBuffer();
		for (String key:fills.keySet()) {
			sb.append(fills.get(key));
		}
		return sb;
	}
}
