package de.keawe.HouseCreator;

import org.json.simple.JSONObject;

public class Roof {

	private double height;
	private RoofShape eastWest;
	private RoofShape northSouth;
	private String fill = "on";
	private String visibility = "normal";

	public Roof(JSONObject json) {
		for (Object key : json.keySet()) {
			if (key instanceof String) {
				String keyString = (String) key;
				Object val = json.get(key);
				switch (keyString) {
					case "height":
						height = Double.parseDouble(""+val);
						break;
					case "east-west":
						eastWest = new RoofShape(height,(JSONObject) val);
						break;
					case "north-south":
						northSouth = new RoofShape(height,(JSONObject) val);
						break;
					case "fill":
						fill = val.toString();
						break;
					case "visibility":
						visibility = val.toString();
						break;
					default: 
						System.out.println("Roof constructor encountered unknown tag: "+key);
				}
				
			}
		}
	}

	public String scadPlus(String name) {
		if (fill.equals("on") || fill.equals("yes")) return name+"_roof();";
		StringBuffer sb = new StringBuffer();
		sb.append("difference(){\n");
		sb.append("	"+name+"_roof();\n");
		sb.append("	translate([0,0,0.05])scale([0.9,0.9,0.9]) "+name+"_roof();\n");
		sb.append("}");
		return sb.toString();
	}

	public void setLength(double length) {
		northSouth.setLength(length+eastWest.overlap());
		eastWest.setWidth(length+eastWest.overlap());
	}

	public void setWidth(double width) {
		northSouth.setWidth(width+northSouth.overlap());
		eastWest.setLength(width+northSouth.overlap());
	}
	
	public StringBuffer scadModule(String name) {
		StringBuffer sb = new StringBuffer();
		sb.append("module "+name+"_roof(){\n");
		sb.append("	intersection(){\n");
		sb.append("		"+northSouth.scad(height)+"\n");
		sb.append("		rotate([0,0,90]) "+eastWest.scad(height)+"\n");
		sb.append("	}\n");
		sb.append("}\n");
		return sb;
	}

	public boolean visible() {
		return !visibility.equals("hidden");
	}
}
