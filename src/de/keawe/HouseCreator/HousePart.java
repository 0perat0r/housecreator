package de.keawe.HouseCreator;
	
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

import java.util.Collection;
import java.util.HashMap;
import java.util.Vector;

import javax.naming.ConfigurationException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
	
	public class HousePart {

		private static HashMap<String,HousePart> registry = new HashMap<>();
		private double length = 10;
		private double width = 10;
		private double offset = 0;
		private String name = null;
		private Vector<HouseLevel> levels;
		private String reference = null;
		private String direction = null;
		private Roof roof;
		private String visibility = "visible";
		
		public HousePart(String name, JSONObject json) throws ConfigurationException {
			this.name = name;
			for (Object key: json.keySet()) {
				if (key instanceof String) {
					String keyString = (String) key;
					
					Object val = json.get(key);
					
					switch (keyString) {
						case "width":
							this.width  = Double.parseDouble(""+val);
							break;
						case "length":
							this.length  = Double.parseDouble(""+val);
							break;
						case "levels":
							loadLevels((JSONArray)val);
							break;
						case "east-of":
							reference = val.toString();
							direction = "east";
							break;
						case "north-of":
							reference = val.toString();
							direction = "north";
							break;
						case "south-of":
							reference = val.toString();
							direction = "south";
							break;
						case "west-of":
							reference = val.toString();
							direction = "west";
							break;
						case "offset":
							offset = Double.parseDouble(""+val);
							break;
						case "visibility":
							visibility  = val.toString();
							break;
						case "roof":
							roof = new Roof((JSONObject) val);
							break;
						default:
							System.out.println("Encountered tag \""+keyString+"\" in house part!");
					}
				}
			}
		}
		
		private void loadLevels(JSONArray json) throws ConfigurationException {
			levels = new Vector<HouseLevel>();
			for (Object level:json) {
				if (level instanceof JSONObject) {
					levels.add(new HouseLevel(length,width,(JSONObject) level));
				} else {
					// add cornice
				}
			}
		}
		
		@Override
		public String toString() {
			StringBuffer sb = new StringBuffer();
			sb.append(name+"-Part ("+length+" x "+width+"m) with "+levels.size()+" levels:\n");
			for (HouseLevel level:levels) sb.append(level+"\n");
			return sb.toString();
		}
		
		public String scadMinus() {
			StringBuffer sb = new StringBuffer();
			double y = 0.0;
			for (HouseLevel level:levels) {
				y+=level.height()/2;
				if (level.visible()) {
					sb.append("translate([0,0,"+y+"]) { // level \""+level.name()+"\"\n\t");
					sb.append(level.scadMinus().replace("\n", "\n\t"));
					sb.append("\n}\n");
				}
				y+=level.height()/2;
			}
			return sb.toString();
		}

		public String scadPlus() {
			StringBuffer sb = new StringBuffer();
			double y = 0.0;
			for (HouseLevel level:levels) {
				y+=level.height()/2;
				if (level.visible()) {
					sb.append("translate([0,0,"+y+"]) { // level \""+level.name()+"\"\n\t");
					sb.append(level.scadPlus().replace("\n", "\n\t"));
					sb.append("\n} // end of \""+level.name()+"\"\n");
				}
				y+=level.height()/2;
			}
			if (roof.visible())	sb.append("translate([0,0,"+y+"]) "+roof.scadPlus(name));
			return sb.toString();
		}

		public static void register(HousePart housePart) {
			registry.put(housePart.name, housePart);
		}

		public static HousePart main() {
			return registry.get("main");
		}

		public Collection<HousePart> adjacent() {
			return registry.values();
		}

		public double x() {
			if (reference == null) return 0;
			HousePart center = registry.get(reference);
			double result = center.x();
			switch (direction) {
				case "east": return result+(length+center.length)/2;
				case "north": return result + offset;
				case "south": return result - offset;
				case "west": return result-(length+center.length)/2;
			}
			return result;
		}
		
		public double y() {
			if (reference == null) return 0;
			HousePart center = registry.get(reference);
			double result = center.y();
			switch (direction) {
				case "east": return result - offset; 
				case "north": return result + (width+center.width)/2; 
				case "south": return result - (width+center.width)/2; 
				case "west": return result  + offset; 
			}
			return result;
		}

		public static Collection<HousePart> list() {
			return registry.values();
		}

		public String name() {
			return name;
		}

		public static void setDefaultWallThickness(double defaultWallThickness) {
			System.out.println("setDefaultWallThickness("+defaultWallThickness+")");
			for (String key:registry.keySet()) {
				HousePart part = registry.get(key);
				for (HouseLevel level:part.levels) level.setDefaultWallThickness(defaultWallThickness);
				part.roof.setLength(part.length+defaultWallThickness);
				part.roof.setWidth(part.width+defaultWallThickness);

			}
		}

		public boolean visible() {
			return !visibility.equals("hidden");
		}

		public StringBuffer roofDefinition() {
			return roof.scadModule(name);
		}
	}
