width=7;
length=10;
height=3;
wall_thickness=0.2;

module outer_wall(){
	color([0.2,0.2,0.2]) difference(){
		cube([length+wall_thickness,width+wall_thickness,height],true);
		translate([0,0,0.1])cube([length-wall_thickness,width-wall_thickness,height],true);
	}
}

module inner_walls(length,width,height,wall_thickness){
			translate([ 0,width/6,0])cube([length,wall_thickness,height],true);
			translate([ length/5,-width/6,0])cube([wall_thickness,4*width/6,height],true);
			translate([-length/5,-width/6,0])cube([wall_thickness,4*width/6,height],true);
			translate([ 0,2*width/6,0])cube([wall_thickness,2*width/6,height],true);
}

outer_wall();
inner_walls(length,width,height,wall_thickness);