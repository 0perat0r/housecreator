height = 5;
width = 10;
length = 15;

polyhedron(
	points=[
		[-length/2,-width/2,0], [length/2,-width/2,0], [length/2,width/2,0], [-length/2,width/2,0],
		[-length/2,-width/3,height/2], [length/2,-width/3,height/2], [length/2,width/3,height/2], [-length/2,width/3,height/2],
		[-length/2,0,height], [length/2,0,height]
	],
	faces=[
		[0,1,2,3], // base
		[0,4,5,1],
		[5,4,8,9],
		[9,8,7,6],
		[6,7,3,2],
		[2,1,5,6],
		[6,5,9],
		[6,5,9],
		[0,3,7,4],
		[7,8,4]
	]
);