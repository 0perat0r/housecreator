wall_thickness=0.4;
wall_heigth = 3;

module wall(){
	color([0.2,0.2,0.2])cube([4,wall_thickness,3],true);
	plus(wall_thickness,wall_heigth);
}

module plus(wall_thickness,wall_height){
	translate([ 0,0.05, 1.2-wall_height/2]) cube([1.3,wall_thickness+0.1,2.4],true);
}

module minus(wall_thickness,wall_height){ // door 1
	translate([ 0,wall_thickness/2, 1.1-wall_height/2])cube([1.1,0.25,2.2],true);
}

difference(){
	wall();
	minus(wall_thickness,wall_heigth);
}
