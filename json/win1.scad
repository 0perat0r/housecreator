wall_thickness=0.4;
module wall(){
	color([0.2,0.2,0.2])cube([4,wall_thickness,3],true);
	plus(wall_thickness);
}

module plus(wall_thickness){
	translate([ 0   ,0.05,0.1 ]) cube([1,   wall_thickness,1.5],true);
	translate([-0.45,0.05,0.8]) cube([0.15,wall_thickness,0.15],true);
	translate([ 0.45,0.05,0.8]) cube([0.15,wall_thickness,0.15],true);
	translate([ 0,0.15,  -0.6])   cube([1.1,wall_thickness,0.1],true);
}

module minus(wall_thickness){
	translate([ 0   ,0,0.1 ]) cube([0.8,wall_thickness+0.25,1.3],true);
}

difference(){
	wall();
	minus(wall_thickness);
}
