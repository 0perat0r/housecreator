wall_thickness=0.4;
wall_heigth = 3;

module wall(){
	color([0.2,0.2,0.2])cube([4,wall_thickness,3],true);
	plus(wall_thickness,wall_heigth);
}

module plus(wall_thickness,wall_height){
	translate([ 0,0.05, 1-wall_height/2]) cube([2.2,wall_thickness+0.1,2],true);
}

module minus(wall_thickness,wall_height){ // door 1
	translate([ 0.5,0.05, 0.9-wall_height/2])cube([0.9,wall_thickness+0.25,1.9],true);
	translate([-0.5,0.05, 0.9-wall_height/2])cube([0.9,wall_thickness+0.25,1.9],true);
}

difference(){
	wall();
	minus(wall_thickness,wall_heigth);
}
