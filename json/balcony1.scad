wt = 0.4;
wh = 2.8;

module wall(){
	color([0.2,0.2,0.2])cube([4,wt,wh],true);
	plus(wt,wh);
}

module plus(wall_thickness,wall_height){
	translate([ 0, 0.8, 0.5-wall_height/2]) cube([3,wall_thickness+1.4,1],true);
}

module minus(wall_thickness,wall_height){ // door 1
	translate([ 0,0.4, 1.3-wall_height/2]) cube([2.8,wall_thickness+2,2.2],true);
}

difference(){
	wall();
	minus(wt,wh);
}