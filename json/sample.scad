$fn=32;


module door2_plus(wall_thickness,wall_height){
	translate([ 0,0.05, 1-wall_height/2]) cube([2.2,wall_thickness+0.1,2],true);
}

module door2_minus(wall_thickness,wall_height){ // door 1
	translate([ 0.5,0.05, 0.9-wall_height/2])cube([0.9,wall_thickness+0.25,1.9],true);
	translate([-0.5,0.05, 0.9-wall_height/2])cube([0.9,wall_thickness+0.25,1.9],true);
}

module win2_plus(wall_thickness){
	intersection(){
		translate([0,0.05,-0.38]) rotate([90,0,0]) cylinder(wall_thickness,1.3,1.3,true);
		translate([0,0.05,0.2])cube([1,   wall_thickness,1.7],true);
	}
	translate([-0.45,0.05,0.8]) cube([0.15,wall_thickness,0.15],true);
	translate([ 0.45,0.05,0.8]) cube([0.15,wall_thickness,0.15],true);
	translate([ 0,0.15,  -0.6])   cube([1.1,wall_thickness,0.1],true);
}

module win2_minus(wall_thickness){
	intersection(){
		translate([0,0.05,-0.38]) rotate([90,0,0]) cylinder(wall_thickness+0.25,1.2,1.2,true);
		translate([0,0.05,0.2]) cube([0.8,wall_thickness+0.25,1.3],true);
	}
}

module win1_plus(wall_thickness){
	translate([ 0   ,0.05,0.1 ]) cube([1,   wall_thickness,1.5],true);
	translate([-0.45,0.05,0.8]) cube([0.15,wall_thickness,0.15],true);
	translate([ 0.45,0.05,0.8]) cube([0.15,wall_thickness,0.15],true);
	translate([ 0,0.15,  -0.6])   cube([1.1,wall_thickness,0.1],true);
}

module win1_minus(wall_thickness){
	translate([ 0   ,0,0.1 ]) cube([0.8,wall_thickness+0.25,1.3],true);
}

module door1_plus(wall_thickness,wall_height){
	translate([ 0,0.05, 1.2-wall_height/2]) cube([1.3,wall_thickness+0.1,2.4],true);
}

module door1_minus(wall_thickness,wall_height){ // door 1
	translate([ 0,wall_thickness/2, 1.1-wall_height/2])cube([1.1,0.25,2.2],true);
}

module balcony1_plus(wall_thickness,wall_height){
	translate([ 0, 0.8, 0.5-wall_height/2]) cube([3,wall_thickness+1.4,1],true);
}

module balcony1_minus(wall_thickness,wall_height){ // door 1
	translate([ 0,0.4, 1.3-wall_height/2]) cube([2.8,wall_thickness+2,2.2],true);
}

module block(length, width, height){
	translate([-length/2,-width/2,0])cube([length,width,height]);
}
module shape1(length, width, height){
	polyhedron(
		points=[
			[-length/2,-width/2,0], [length/2,-width/2,0], [length/2,width/2,0], [-length/2,width/2,0], [-length/2,0,height], [length/2,0,height]
		],
		faces=[
			[0,1,2,3],[5,4,3,2],[0,4,5,1],[0,3,4],[5,2,1]
		]
	);
}
module shape2(length, width, height){
	polyhedron(
		points=[
			[-length/2,-width/2,0], [length/2,-width/2,0], [length/2,width/2,0], [-length/2,width/2,0],
			[-length/2,-width/3,height/2], [length/2,-width/3,height/2], [length/2,width/3,height/2], [-length/2,width/3,height/2],
			[-length/2,0,height], [length/2,0,height]
		],
		faces=[
			[0,1,2,3], // base
			[0,4,5,1],
			[5,4,8,9],
			[9,8,7,6],
			[6,7,3,2],
			[2,1,5,6],
			[6,5,9],
			[6,5,9],
			[0,3,7,4],
			[7,8,4]
		]
	);
}
module main_roof(){
	intersection(){
		shape1(13.8, 10.8, 4.0);
		rotate([0,0,90]) shape2(10.8, 13.8, 4.0);
	}
}
module side2_roof(){
	intersection(){
		block(4.3, 4.8, 2.0);
		rotate([0,0,90]) shape2(4.8, 4.3, 2.0);
	}
}
module side1_roof(){
	intersection(){
		shape1(8.3, 8.8, 2.5);
		rotate([0,0,90]) block(8.8, 8.3, 2.5);
	}
}
module inner2_walls(length,width,height,wall_thickness){
			translate([ 0,width/6,0])cube([length,wall_thickness,height],true);
			translate([ length/5,-width/6,0])cube([wall_thickness,4*width/6,height],true);
			translate([-length/5,-width/6,0])cube([wall_thickness,4*width/6,height],true);
			translate([ 0,2*width/6,0])cube([wall_thickness,2*width/6,height],true);
}
module inner1_walls(length,width,height,wall_thickness){
	difference(){
		union(){
			cube([length,wall_thickness,height],true);
			translate([ length/8,0,0])cube([wall_thickness,width,height],true);
			translate([-length/8,0,0])cube([wall_thickness,width,height],true);
			cube([3*length/7+wall_thickness,3*width/7+wall_thickness,height],true);
		}
		translate([0,0,0.1])cube([3*length/7-wall_thickness,3*width/7-wall_thickness,height],true);
	}
}
scale([8.333333333333334,8.333333333333334,8.333333333333334]) color([0.9,0.9,0.9]) difference(){
	union(){
		translate([0.0, 0.0, 0]) { // main
			translate([0,0,8.600000000000001]) difference(){
				main_roof();
				translate([0,0,0.05])scale([0.9,0.9,0.9]) main_roof();
			}
		}
	} // end of union. subtracted parts follow:
	translate([0.0, 0.0, 0]) { // main
		
	}
	translate([-12.0, 6.0, 0]) { // side2
		translate([0,0,1.25]) { // level "unnamed level"
			translate([0,2.0,0]) { // north wall 
				translate([-1.0,0,0]) win1_minus(0.3, 2.5);
				translate([1.0,0,0]) win1_minus(0.3, 2.5);
			}
			translate([2.0,0 ,0]) rotate([0,0,-90]) { // east wall
				translate([0.0,0,0]) win1_minus(0.3, 2.5);
			}
			translate([0,-2.0,0]) rotate([0,0,180]) { // south wall
				translate([0.0,0,0]) door2_minus(0.3, 2.5);
			}
			translate([-2.0,0 ,0]) rotate([0,0,90]) { // west wall
				translate([0.0,0,0]) win1_minus(0.3, 2.5);
			}
		}
	}
	translate([-10.5, 0.0, 0]) { // side1
		translate([0,0,1.75]) { // level "unnamed level"
			translate([0,4.0,0]) { // north wall 
				translate([2.0,0,0]) win1_minus(0.3, 3.5);
			}
			translate([4.0,0 ,0]) rotate([0,0,-90]) { // east wall
				
			}
			translate([0,-4.0,0]) rotate([0,0,180]) { // south wall
				translate([-2.666666666666667,0,0]) win1_minus(0.3, 3.5);
				translate([-4.440892098500626E-16,0,0]) win1_minus(0.3, 3.5);
				translate([2.666666666666666,0,0]) win1_minus(0.3, 3.5);
			}
			translate([-4.0,0 ,0]) rotate([0,0,90]) { // west wall
				translate([-2.0,0,0]) win1_minus(0.3, 3.5);
				translate([2.0,0,0]) win1_minus(0.3, 3.5);
			}
		}
	}
}