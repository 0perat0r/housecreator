height = 5;
width = 10;
length = 15;

polyhedron(
	points=[
		[-length/2,-width/2,0], [length/2,-width/2,0], [length/2,width/2,0], [-length/2,width/2,0], [-length/2,0,height], [length/2,0,height]
	],
	faces=[
		[0,1,2,3],[5,4,3,2],[0,4,5,1],[0,3,4],[5,2,1]
	]
);