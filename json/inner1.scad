width=7;
length=10;
height=3;
wall_thickness=0.2;

module outer_wall(){
	color([0.2,0.2,0.2]) difference(){
		cube([length+wall_thickness,width+wall_thickness,height],true);
		translate([0,0,0.1])cube([length-wall_thickness,width-wall_thickness,height],true);
	}
}

module inner_walls(length,width,height,wall_thickness){
	difference(){
		union(){
			cube([length,wall_thickness,height],true);
			translate([ length/8,0,0])cube([wall_thickness,width,height],true);
			translate([-length/8,0,0])cube([wall_thickness,width,height],true);
			cube([3*length/7+wall_thickness,3*width/7+wall_thickness,height],true);
		}
		translate([0,0,0.1])cube([3*length/7-wall_thickness,3*width/7-wall_thickness,height],true);
	}
}

outer_wall();
inner_walls(length,width,height,wall_thickness);